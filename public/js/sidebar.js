$(document).ready(function() {
    // banner slide
    $('#sidebar-program').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        vertical: true,
        infinite: true,
        autoplay: true,
        dots: false,
        arrows: false,
        speed: 500,
        autoplaySpeed: 5000,
        //lazyLoad: 'ondemand',
        cssEase: 'linear'
    });

});
