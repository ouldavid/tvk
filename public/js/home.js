$(document).ready(function() {
    // banner slide
    $('#banner-slide').slick({
        slidesToShow: 1,
        infinite: true,
        autoplay: false,
        dots: false,
        arrows: false,
        speed: 500,
        autoplaySpeed: 5000,
        //lazyLoad: 'ondemand',
        cssEase: 'linear'
    });

    // programs
    $('.program').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        infinite: true,
        autoplay: false,
        dots: false,
        arrows: true,
        nextArrow: '<button class="right d-flex align-items-center bg-transparent border-0"><img src="../img/arrow/arrow.png" alt="arrow right"></button>',
        prevArrow: '<button class="left d-flex align-items-center bg-transparent border-0"><img src="../img/arrow/arrow.png" alt="arrow left"></button>',
        speed: 500,
        autoplaySpeed: 5000,
        //lazyLoad: 'ondemand',
        cssEase: 'linear',
        responsive: [
            {
                breakpoint: 767.98,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 575.98,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    autoplay: true,
                }
            }
        ]
    });

});
