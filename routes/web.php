<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// define global variable
view()->share('version', time());
view()->share('host_name', 'ទូរទស្សន៍ជាតិកម្ពុជា ទទក - TVK');

// Views
Route::get('/', 'HomeController@index')->name('home');
Route::get('/pressrelease', 'PressreleaseController@index')->name('pressrelease');
Route::get('/program', 'ProgramController@index')->name('program');
Route::get('/program/category', 'ProgramController@category')->name('program-category');
Route::get('/event', 'EventController@index')->name('event');