<div id="container-sidebar">
    <div class="mx-3 py-3">
        <h2 class="header-title text-center color-black py-3 mb-3">ថ្មីៗពីទទក</h2>
        @for($i=1;$i<=5;$i++)
        <div class="card card-horizontal mb-2">
            <a href="#" class="img-wrap-lazy">
                <img class="card-img-top b-lazy" data-src="{{ asset('img/banner_01.jpg') }}" alt="{{ $host_name }}">
            </a>
            <div class="card-body p-2">
                <a href="#">
                    <h5 class="card-title mb-0">គណៈប្រតិភូកម្ពុជាសូមប្រើប្រាស់សិទ្ធិឆ្លើយតបទៅនិងគណៈប្រតិភូនៃចក្រភពអង់គ្លេសដែលបានថ្លែងពាក់ព័ន្ធនឹងស្ថានភាពសេរីភាព បញ្ចេញមតិនិងលទ្ធិប្រជាធិបតេយ្យនៅកម្ពុជា</h5>
                </a>
            </div>
        </div>
        @endfor

        <h2 class="header-title text-center color-black py-3 mb-3">កម្មវិធីទទក</h2>
        <div class="row">
            <div class="col-12">
                <div id="sidebar-program">
                    <div class="slide mb-2">
                        <a href="#"><img class="img-fluid" src="{{ asset('img/programs/kun-knhom.jpg') }}" alt="kun-knhom" /></a>
                    </div>
                    <div class="slide mb-2">
                        <a href="#"><img class="img-fluid" src="{{ asset('img/programs/mit-don-trey-kor.jpg') }}" alt="mit-don-trey-kor" /></a>
                    </div>
                    <div class="slide mb-2">
                        <a href="#"><img class="img-fluid" src="{{ asset('img/programs/sbai-reatry.jpg') }}" alt="sbai-reatry" /></a>
                    </div>
                    <div class="slide mb-2">
                        <a href="#"><img class="img-fluid" src="{{ asset('img/programs/sbai-reatry.jpg') }}" alt="sbai-reatry" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
