{{--<div class="container-fluid p-0 d-none d-lg-block bg-blue-dark">
    <div class="container py-md-3 py-0">
        <div class="row">
            <div class="col-md-12">
                <div id="logo" class="text-center">
                    <a href="/"><img class="img-fluid" src="{{ asset('img/logo.png') }}" alt="{{ $host_name }}" /></a>
                </div>
            </div>
        </div>
    </div>
</div>--}}
<nav id="navbar" class="navbar navbar-expand-lg navbar-dark bg-dark sidebarNavigation" data-sidebarClass="navbar-dark bg-dark">
    <div class="container-xl container-fluid">
        {{--<a href="/" class="second-brand d-flex justify-content-center mb-0">
            <img src="{{ asset('/img/mobile_logo.png') }}" alt="{{ $host_name }} Logo" class="brand-image elevation-3">
            --}}{{--<span class="brand-text font-weight-light text-uppercase">ទូរទស្សន៍ជាតិកម្ពុជា</span>--}}{{--
        </a>--}}
        <a href="/" class="navbar-brand py-0">
            <img src="{{ asset('/img/logo.png') }}" alt="{{ $host_name }} Logo" class="brand-image elevation-3 img-fluid">
        </a>
        <button class="navbar-toggler leftNavbarToggler" type="button" data-toggle="" data-target="#navbarsExampleDefault"
                aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <button type="button" class="close d-lg-none" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <a href="/" class="second-brand d-flex justify-content-center d-lg-none mb-5">
                <img src="{{ asset('/img/logo.png') }}" alt="{{ $host_name }} Logo" class="brand-image elevation-3">
                {{--<span class="brand-text font-weight-light text-uppercase">Poraman</span>--}}
            </a>
            <ul class="nav navbar-nav nav-flex-icons m-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">
                        <i class="{{ config('global.icon_home') }}"></i> <span>ទំព័រដើម</span>
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span>ព័ត៌មាន</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span>កម្មវិធីទទក</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span>ព្រឹត្តិការណ៍</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span>អំពីទទក</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <span>ទំនាក់ទំនង</span>
                    </a>
                </li>
            </ul><!-- .navbar-nav -->
            <!-- social mobile -->
            <ul class="social d-lg-none">
                <li class="nav-item mx-2">
                    <a href="#" target="_blank" class="icon-button facebook"><i class="fab fa-facebook-f"></i><span></span></a>
                </li>
                <li class="nav-item mx-2">
                    <a href="#" target="_blank" class="icon-button bg-youtube youtube"><i class="fab fa-youtube"></i><span></span></a>
                </li>
                <li class="nav-item mx-2">
                    <a href="#" target="_blank" class="icon-button instagram"><i class="fab fa-instagram"></i><span></span></a>
                </li>
            </ul>
        </div><!-- #navbarsExampleDefault -->

        <div class="nav-right d-flex">
            <ul class="social d-inline-block mb-0">
                <li class="nav-item">
                    <a href="#" target="_blank" class="icon-button facebook"><i class="fab fa-facebook-f"></i><span></span></a>
                </li>
                <li class="nav-item">
                    <a href="#" target="_blank" class="icon-button bg-instagram instagram"><i class="fab fa-instagram"></i><span></span></a>
                </li>
                <li class="nav-item">
                    <a href="#" target="_blank" class="icon-button bg-youtube youtube"><i class="fab fa-youtube"></i><span></span></a>
                </li>
            </ul>
            <ul class="d-inline-block mb-0">
                <li class="nav-item pr-0">
                    <button id="search-btn"><i class="fas fa-search"></i></button>
                </li>
            </ul>
        </div>

    </div>
</nav>

<!-- search overlay -->
<div class="search-overlay">
    <button id="close-btn" type="button" class="close" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <form action="">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 col-sm-12 offset-sm-0">
                    <div class="input-group form-btn">
                        <input type="text" name="txt_search" class="form-control border-bottom" placeholder="Search here..." aria-describedby="button-submit">
                        <div class="input-group-append">
                            <button class="btn btn-outline-light brd-left-none border-bottom" type="button" id="button-submit"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

