<div id="footer" class="container-fluid py-4 px-0">
    <div class="container">
        <div class="row">
            <div class="col-md-4 d-flex justify-content-center justify-content-md-start hr">
                <ul>
                    <li class="d-table mx-auto">
                        <a href="/" class="second-brand mb-3">
                            <img src="{{ asset('/img/logo.png') }}" alt="{{ $host_name }} Logo" class="brand-image elevation-3">
                            <span class="brand-text font-weight-light text-uppercase">ទូរទស្សន៍ជាតិកម្ពុជា</span>
                        </a>
                    </li>
                    <li class="d-inline-block d-md-block mr-3 mr-md-0">
                        <i class="far fa-envelope"></i>
                        <a href="mailto:info@poraman.com">info@poraman.com</a>
                    </li>
                    <li class="d-inline-block d-md-block">
                        <i class="fas fa-phone-square-alt"></i>
                        <a href="tel:0972323239">097 23 23 239</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 d-flex justify-content-center hr">
                <ul>
                    <li>
                        <h4 class="text-uppercase text-center text-md-left mb-3">{{ $host_name }}</h4>
                    </li>
                    <li class="d-inline-block d-md-block mr-3 mr-md-0">
                        <i class="fas fa-caret-right"></i>
                        <a href="#">អំពីយើង</a>
                    </li>
                    <li class="d-inline-block d-md-block">
                        <i class="fas fa-caret-right"></i>
                        <a href="#">ទាក់ទងយើង</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 d-flex justify-content-center justify-content-md-end">
                <ul class="social">
                    <li class="d-block w-auto h-auto text-center text-md-left">
                        <h4 class="text-uppercase mb-3">បណ្តាញសង្គម</h4>
                    </li>
                    <li class="nav-item mx-2">
                        <a href="https://www.facebook.com/PoramanCambodia" target="_blank" class="icon-button facebook"><i class="fab fa-facebook-f"></i><span></span></a>
                    </li>
                    <li class="nav-item mx-2">
                        <a href="https://www.youtube.com/c/PoramanCambodiaOfficial" target="_blank" class="icon-button bg-youtube youtube"><i class="fab fa-youtube"></i><span></span></a>
                    </li>
                    <li class="nav-item mx-2">
                        <a href="https://www.instagram.com/PoramanCambodia" target="_blank" class="icon-button instagram"><i class="fab fa-instagram"></i><span></span></a>
                    </li>
                    <li class="nav-item mx-2">
                        <a href="https://twitter.com/PoramanC" target="_blank" class="icon-button twitter"><i class="fab fa-twitter"></i><span></span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="copy">
        <p class="text-center text-uppercase mb-0">&copy; រក្សាសិទ្ធិគ្រប់យ៉ាងដោយ {{ $host_name }} ឆ្នាំ២០២០</p>
    </div>
</div><!-- Footer -->
