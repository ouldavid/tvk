@extends('layouts.app')

@section('title', Str::replaceFirst('-', ' ', ucfirst(Route::currentRouteName())))
@section('social-title', $host_name)
@section('description', 'Welcome to the official home National Television of Cambodia (TVK) on Facebook. Our mission is Provide facts from Cambodia and internationally. To inform, educate and entertain.')

@section('css')
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

@section('content')
    <div class="bg-blue-dark">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="header-title line double-razor"><a href="#">កម្មវិធីទទក</a></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-gradient">
        <div class="container">
            <div class="bg-block py-3 px-md-3 px-0">
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <a href="#" class="img-wrap-lazy lazy-square">
                            <img class="img-fluid b-lazy" data-src="{{ asset('img/programs/kun-knhom.jpg') }}" alt="kun-knhom" />
                        </a>
                    </div>
                    <div class="col-md-4 mb-3">
                        <a href="#" class="img-wrap-lazy lazy-square">
                            <img class="img-fluid b-lazy" data-src="{{ asset('img/programs/mit-don-trey-kor.jpg') }}" alt="mit-don-trey-kor" />
                        </a>
                    </div>
                    <div class="col-md-4 mb-3">
                        <a href="#" class="img-wrap-lazy lazy-square">
                            <img class="img-fluid b-lazy" data-src="{{ asset('img/programs/sbai-reatry.jpg') }}" alt="sbai-reatry" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- page js -->
    {{--<script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>--}}
@endsection
