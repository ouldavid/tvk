@extends('layouts.app')

@section('title', Str::replaceFirst('-', ' ', ucfirst(Route::currentRouteName())))
@section('social-title', $host_name)
@section('description', 'Welcome to the official home National Television of Cambodia (TVK) on Facebook. Our mission is Provide facts from Cambodia and internationally. To inform, educate and entertain.')

@section('css')
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/program.css?v='.$version) }}">
@endsection

@section('content')
    <div class="bg-blue-dark">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2 class="header-title line double-razor"><a href="#">កម្មវិធី កុនខ្ញុំ</a></h2>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-gradient">
        <div class="container bg-block-sm">
            <div class="bg-block py-3 px-md-3 px-0">
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <div class="img-wrap-lazy lazy-square shadow-sm">
                            <img class="img-fluid b-lazy" data-src="{{ asset('img/programs/kun-knhom.jpg') }}" alt="kun-knhom" />
                        </div>
                    </div>
                    <div class="col-md-8 mb-3">
                        <p>គណៈប្រតិភូកម្ពុជាសូមប្រើប្រាស់សិទ្ធិឆ្លើយតបទៅនិងគណៈប្រតិភូនៃចក្រភពអង់គ្លេសដែលបានថ្លែងពាក់ព័ន្ធនឹងស្ថានភាពសេរីភាព បញ្ចេញមតិនិងលទ្ធិប្រជាធិបតេយ្យនៅកម្ពុជា</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <hr>
                        <div class="eps">
                            <ul>
                                <li>
                                    <span class="btn-sm bg-blue-dark text-white border-0 rounded-0 mr-3">12-Jul-20</span>
                                    <a href="#" class="">my kun eps-01</a>
                                </li>
                                <li>
                                    <span class="btn-sm bg-blue-dark text-white border-0 rounded-0 mr-3">11-Jul-20</span>
                                    <a href="#" class="">my kun eps-02</a>
                                </li>
                                <li>
                                    <span class="btn-sm bg-blue-dark text-white border-0 rounded-0 mr-3">10-Jul-20</span>
                                    <a href="#" class="">my kun eps-03</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- page js -->
    {{--<script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>--}}
@endsection
