@extends('layouts.app')

@section('title', Str::replaceFirst('-', ' ', ucfirst(Route::currentRouteName())))
@section('social-title', $host_name)
@section('description', 'Welcome to the official home National Television of Cambodia (TVK) on Facebook. Our mission is Provide facts from Cambodia and internationally. To inform, educate and entertain.')

@section('css')
    <!-- slick slider -->
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick-theme.css') }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

@section('banner')
    <div class="bg-blue-dark">
        <div class="container py-3">
            <div class="row">
                <div class="col-md-8 pr-md-2 px-xs-0 mb-sm-13">
                    <div id="banner-slide">
                        <div class="slide">
                            <a href="#">
                                <img class="img-fluid" src="img/banner_01.jpg" alt="{{ $host_name }}" />
                            </a>
                            <div class="carousel-caption p-0">
                                <a href="#" class="btn-sm tags bg-red mx-3">ថ្មីៗពីទទក</a>
                                <div class="bg-caption py-2 px-3 py-md-3">
                                    <h3>គណៈប្រតិភូកម្ពុជាសូមប្រើប្រាស់សិទ្ធិឆ្លើយតបទៅនិងគណៈប្រតិភូនៃចក្រភពអង់គ្លេសដែលបានថ្លែងពាក់ព័ន្ធនឹងស្ថានភាពសេរីភាព បញ្ចេញមតិនិងលទ្ធិប្រជាធិបតេយ្យនៅកម្ពុជា</h3>
                                </div>
                            </div>
                        </div>
                        <div class="slide">
                            <a href="#">
                                <img class="img-fluid" src="img/banner_01.jpg" alt="{{ $host_name }}" />
                            </a>
                            <div class="carousel-caption p-0">
                                <a href="#" class="btn-sm tags bg-red mx-3">ថ្មីៗពីទទក</a>
                                <div class="bg-caption py-2 px-3 py-md-3">
                                    <h3>គណៈប្រតិភូកម្ពុជាសូមប្រើប្រាស់សិទ្ធិឆ្លើយតបទៅនិងគណៈប្រតិភូនៃចក្រភពអង់គ្លេសដែលបានថ្លែងពាក់ព័ន្ធនឹងស្ថានភាពសេរីភាព</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 pl-md-2">
                    <div class="row">
                        <div class="col-md-12 mb-13">
                            <a href="#">
                                <img class="img-fluid" src="img/live_01.jpg" alt="{{ $host_name }}" />
                            </a>
                        </div>
                        <div class="col-md-12">
                            <a href="#">
                                <img class="img-fluid" src="img/live_01.jpg" alt="{{ $host_name }}" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="bg-gradient">
        <div class="container py-3">
            <h2 class="header-title line double-razor"><a href="#">ថ្មីៗពីទទក</a></h2>
            <div class="row">
                <div class="col-md-4 mb-3">
                    <div class="card">
                        <a href="#" class="img-wrap-lazy">
                            <img class="card-img-top b-lazy" data-src="{{ asset('img/banner_01.jpg') }}" alt="{{ $host_name }}">
                        </a>
                        <div class="card-body p-3">
                            <a href="#">
                                <h5 class="card-title mb-0">គណៈប្រតិភូកម្ពុជាសូមប្រើប្រាស់សិទ្ធិឆ្លើយតបទៅនិងគណៈប្រតិភូនៃចក្រភពអង់គ្លេសដែលបានថ្លែងពាក់ព័ន្ធនឹងស្ថានភាពសេរីភាព បញ្ចេញមតិនិងលទ្ធិប្រជាធិបតេយ្យនៅកម្ពុជា</h5>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <div class="card card-horizontal-xs">
                        <a href="#" class="img-wrap-lazy">
                            <img class="card-img-top b-lazy" data-src="{{ asset('img/banner_01.jpg') }}" alt="{{ $host_name }}">
                        </a>
                        <div class="card-body p-md-3 p-2">
                            <a href="#">
                                <h5 class="card-title mb-0">គណៈប្រតិភូកម្ពុជាសូមប្រើប្រាស់សិទ្ធិឆ្លើយតបទៅនិងគណៈប្រតិភូនៃចក្រភពអង់គ្លេសដែលបានថ្លែងពាក់ព័ន្ធនឹងស្ថានភាពសេរីភាព បញ្ចេញមតិនិងលទ្ធិប្រជាធិបតេយ្យនៅកម្ពុជា</h5>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-3">
                    <div class="card card-horizontal-xs">
                        <a href="#" class="img-wrap-lazy">
                            <img class="card-img-top b-lazy" data-src="{{ asset('img/banner_01.jpg') }}" alt="{{ $host_name }}">
                        </a>
                        <div class="card-body p-md-3 p-2">
                            <a href="#">
                                <h5 class="card-title mb-0">គណៈប្រតិភូកម្ពុជាសូមប្រើប្រាស់សិទ្ធិឆ្លើយតបទៅនិងគណៈប្រតិភូនៃចក្រភពអង់គ្លេសដែលបានថ្លែងពាក់ព័ន្ធនឹងស្ថានភាពសេរីភាព បញ្ចេញមតិនិងលទ្ធិប្រជាធិបតេយ្យនៅកម្ពុជា</h5>
                            </a>
                        </div>
                    </div>
                </div>
            </div><!-- tvk new -->

            <h2 class="header-title line double-razor"><a href="#">កម្មវិធីទទក</a></h2>
            <div class="row">
                <div class="col-12">
                    <div class="program mb-3">
                        <div class="slide">
                            <a href="#"><img class="img-fluid" src="{{ asset('img/programs/kun-knhom.jpg') }}" alt="kun-knhom" /></a>
                        </div>
                        <div class="slide">
                            <a href="#"><img class="img-fluid" src="{{ asset('img/programs/mit-don-trey-kor.jpg') }}" alt="mit-don-trey-kor" /></a>
                        </div>
                        <div class="slide">
                            <a href="#"><img class="img-fluid" src="{{ asset('img/programs/sbai-reatry.jpg') }}" alt="sbai-reatry" /></a>
                        </div>
                        <div class="slide">
                            <a href="#"><img class="img-fluid" src="{{ asset('img/programs/sbai-reatry.jpg') }}" alt="sbai-reatry" /></a>
                        </div>
                    </div>
                </div>
            </div><!-- .program -->

            <div class="row">
                <div class="col-md-4">
                    <h2 class="header-title line double-razor"><a href="#">ព្រឹត្តិការណ៍</a></h2>
                    @for($i=1;$i<=3;$i++)
                    <div class="card mb-3">
                        <div class="img-wrap-lazy position-relative">
                            <a href="#" class="d-block video-btn" data-toggle="modal" data-src='<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftvkchannel7%2Fvideos%2F1028608787604282%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>' data-target="#modal-vdo">
                                <img class="card-img-top b-lazy" data-src="{{ asset('img/banner_01.jpg') }}" alt="{{ $host_name }}">
                            </a>
                            <a href="#" class="btn-sm tags position-absolute bottom left bg-red">ព្រឹត្តិការណ៍</a>
                        </div>
                        <div class="card-body p-3">
                            <a href="#" class="video-btn" data-toggle="modal" data-src='<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftvkchannel7%2Fvideos%2F1028608787604282%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>' data-target="#modal-vdo">
                                <h5 class="card-title mb-0">គណៈប្រតិភូកម្ពុជាសូមប្រើប្រាស់សិទ្ធិឆ្លើយតបទៅនិងគណៈប្រតិភូនៃចក្រភពអង់គ្លេសដែលបានថ្លែងពាក់ព័ន្ធនឹងស្ថានភាពសេរីភាព បញ្ចេញមតិនិងលទ្ធិប្រជាធិបតេយ្យនៅកម្ពុជា</h5>
                            </a>
                        </div>
                        <a href="#" class="btn-sm btn-play top right mx-3 my-2 video-btn" data-toggle="modal" data-src='<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftvkchannel7%2Fvideos%2F1028608787604282%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>' data-target="#modal-vdo"><i class="fas fa-play"></i></a>
                    </div>
                    @endfor
                </div>
                <div class="col-md-4">
                    <h2 class="header-title line double-razor"><a href="#">កាកបាទក្រហម</a></h2>
                    @for($i=1;$i<=3;$i++)
                        <div class="card mb-3">
                            <div class="img-wrap-lazy position-relative">
                                <a href="#" class="d-block video-btn" data-toggle="modal" data-src='<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftvkchannel7%2Fvideos%2F1028608787604282%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>' data-target="#modal-vdo">
                                    <img class="card-img-top b-lazy" data-src="{{ asset('img/banner_01.jpg') }}" alt="{{ $host_name }}">
                                </a>
                                <a href="#" class="btn-sm tags position-absolute bottom left bg-red"><i class="fas fa-plus"></i></a>
                            </div>
                            <div class="card-body p-3">
                                <a href="#" class="video-btn" data-toggle="modal" data-src='<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftvkchannel7%2Fvideos%2F1028608787604282%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>' data-target="#modal-vdo">
                                    <h5 class="card-title mb-0">គណៈប្រតិភូកម្ពុជាសូមប្រើប្រាស់សិទ្ធិឆ្លើយតបទៅនិងគណៈប្រតិភូនៃចក្រភពអង់គ្លេសដែលបានថ្លែងពាក់ព័ន្ធនឹងស្ថានភាពសេរីភាព បញ្ចេញមតិនិងលទ្ធិប្រជាធិបតេយ្យនៅកម្ពុជា</h5>
                                </a>
                            </div>
                            <a href="#" class="btn-sm btn-play top right mx-3 my-2 video-btn" data-toggle="modal" data-src='<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftvkchannel7%2Fvideos%2F1028608787604282%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>' data-target="#modal-vdo"><i class="fas fa-play"></i></a>
                        </div>
                    @endfor
                </div>
                <div class="col-md-4">
                    <h2 class="header-title line double-razor"><a href="#">កម្ពុជាពិត</a></h2>
                    @for($i=1;$i<=3;$i++)
                        <div class="card mb-3">
                            <div class="img-wrap-lazy position-relative">
                                <a href="#" class="d-block video-btn" data-toggle="modal" data-src='<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftvkchannel7%2Fvideos%2F1028608787604282%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>' data-target="#modal-vdo">
                                    <img class="card-img-top b-lazy" data-src="{{ asset('img/banner_01.jpg') }}" alt="{{ $host_name }}">
                                </a>
                                <a href="#" class="btn-sm tags position-absolute bottom left bg-red">កម្ពុជាពិត</a>
                            </div>
                            <div class="card-body p-3">
                                <a href="#" class="video-btn" data-toggle="modal" data-src='<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftvkchannel7%2Fvideos%2F1028608787604282%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>' data-target="#modal-vdo">
                                    <h5 class="card-title mb-0">គណៈប្រតិភូកម្ពុជាសូមប្រើប្រាស់សិទ្ធិឆ្លើយតបទៅនិងគណៈប្រតិភូនៃចក្រភពអង់គ្លេសដែលបានថ្លែងពាក់ព័ន្ធនឹងស្ថានភាពសេរីភាព បញ្ចេញមតិនិងលទ្ធិប្រជាធិបតេយ្យនៅកម្ពុជា</h5>
                                </a>
                            </div>
                            <a href="#" class="btn-sm btn-play top right mx-3 my-2 video-btn" data-toggle="modal" data-src='<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftvkchannel7%2Fvideos%2F1028608787604282%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>' data-target="#modal-vdo"><i class="fas fa-play"></i></a>
                        </div>
                    @endfor
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal-vdo" tabindex="-1" role="dialog" aria-labelledby="vdoModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <!-- 16:9 aspect ratio -->
                    <div id="video" class="embed-responsive embed-responsive-16by9">
                        <!-- iframe append -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- slick slider -->
    <script type="text/javascript" src="{{ asset('plugins/slick/slick.min.js') }}"></script>
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>
@endsection
