@extends('layouts.app')

@section('title', Str::replaceFirst('-', ' ', ucfirst(Route::currentRouteName())))
@section('social-title', $host_name)
@section('description', 'Welcome to the official home National Television of Cambodia (TVK) on Facebook. Our mission is Provide facts from Cambodia and internationally. To inform, educate and entertain.')

@section('css')
    <!-- slick slider -->
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick-theme.css') }}">
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

@section('content')
    <div class="bg-gradient">
        <div class="bg-blue-dark">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 class="header-title line double-razor"><a href="#">ព្រឹត្តិការណ៍</a></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <!-- content -->
                <div class="col-md-8">
                    <div class="bg-block py-3 px-md-3 px-0">
                        <div class="row">
                            <div class="col-12 mb-3">
                                <div class="card">
                                    <div class="img-wrap-lazy position-relative">
                                        <a href="#" class="d-block video-btn" data-toggle="modal" data-src='<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftvkchannel7%2Fvideos%2F1028608787604282%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>' data-target="#modal-vdo">
                                            <img class="card-img-top b-lazy" data-src="{{ asset('img/banner_01.jpg') }}" alt="{{ $host_name }}">
                                        </a>
                                        <div class="carousel-caption p-0">
                                            <a href="#" class="btn-sm tags bg-red mx-3">ព្រឹត្តិការណ៍</a>
                                            <div class="bg-caption py-2 px-3 py-md-3">
                                                <h3>គណៈប្រតិភូកម្ពុជាសូមប្រើប្រាស់សិទ្ធិឆ្លើយតបទៅនិងគណៈប្រតិភូនៃចក្រភពអង់គ្លេសដែលបានថ្លែងពាក់ព័ន្ធនឹងស្ថានភាពសេរីភាព បញ្ចេញមតិនិងលទ្ធិប្រជាធិបតេយ្យនៅកម្ពុជា</h3>
                                            </div>
                                        </div>
                                        <a href="#" class="btn-sm btn-play top right mx-3 my-2 video-btn" data-toggle="modal" data-src='<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftvkchannel7%2Fvideos%2F1028608787604282%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>' data-target="#modal-vdo"><i class="fas fa-play"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @for($i=1;$i<=6;$i++)
                            <div class="col-12 col-md-6 mb-3">
                                <div class="card">
                                    <div class="img-wrap-lazy position-relative">
                                        <a href="#" class="d-block video-btn" data-toggle="modal" data-src='<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftvkchannel7%2Fvideos%2F1028608787604282%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>' data-target="#modal-vdo">
                                            <img class="card-img-top b-lazy" data-src="{{ asset('img/banner_01.jpg') }}" alt="{{ $host_name }}">
                                        </a>
                                        <div class="carousel-caption p-0">
                                            <a href="#" class="btn-sm tags bg-red mx-3">ព្រឹត្តិការណ៍</a>
                                            <div class="bg-caption py-2 px-3 py-md-3">
                                                <h5>គណៈប្រតិភូកម្ពុជាសូមប្រើប្រាស់សិទ្ធិឆ្លើយតបទៅនិងគណៈប្រតិភូនៃចក្រភពអង់គ្លេសដែលបានថ្លែងពាក់ព័ន្ធនឹងស្ថានភាពសេរីភាព បញ្ចេញមតិនិងលទ្ធិប្រជាធិបតេយ្យនៅកម្ពុជា</h5>
                                            </div>
                                        </div>
                                        <a href="#" class="btn-sm btn-play top right mx-3 my-2 video-btn" data-toggle="modal" data-src='<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Ftvkchannel7%2Fvideos%2F1028608787604282%2F&show_text=0&width=560" width="560" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>' data-target="#modal-vdo"><i class="fas fa-play"></i></a>
                                    </div>
                                </div>
                            </div>
                             @endfor
                        </div>
                    </div>
                </div>
                <!-- sidebar -->
                <div class="col-md-4">
                    <!-- include sidebar -->
                    @include('templates.sidebar')
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal-vdo" tabindex="-1" role="dialog" aria-labelledby="vdoModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <!-- 16:9 aspect ratio -->
                    <div id="video" class="embed-responsive embed-responsive-16by9">
                        <!-- iframe append -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- slick slider -->
    <script type="text/javascript" src="{{ asset('plugins/slick/slick.min.js') }}"></script>
    <!-- sidebar js -->
    <script type="text/javascript" src="{{ asset('js/sidebar.js?v='.$version) }}"></script>
    <!-- page js -->
    {{--<script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>--}}
@endsection
