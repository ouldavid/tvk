@extends('layouts.master')

@section('title', Str::replaceFirst('-', ' ', ucfirst(Route::currentRouteName())))
@section('social-title', $host_name)
@section('description', 'Welcome to the official home National Television of Cambodia (TVK) on Facebook. Our mission is Provide facts from Cambodia and internationally. To inform, educate and entertain.')

@section('css')
    <!-- page css -->
    <link rel="stylesheet" href="{{ asset('css/'.strtolower(Route::currentRouteName()).'.css?v='.$version) }}">
@endsection

@section('content')
    <div class="bg-gradient">
        <div class="bg-blue-dark">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 class="header-title line double-razor"><a href="#">ថ្មីៗពីទទក</a></h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <!-- content -->
                <div class="col-md-8">
                    <div class="bg-block py-3 px-md-3 px-0 mb-3">
                        <div class="row">
                            <div class="col-12 mb-3">
                                <div class="card">
                                    <a href="#">
                                        <img class="card-img-top" src="{{ asset('img/banner_01.jpg') }}" alt="{{ $host_name }}">
                                    </a>
                                    <div class="card-body p-3">
                                        <a href="#">
                                            <h5 class="card-title mb-0">គណៈប្រតិភូកម្ពុជាសូមប្រើប្រាស់សិទ្ធិឆ្លើយតបទៅនិងគណៈប្រតិភូនៃចក្រភពអង់គ្លេសដែលបានថ្លែងពាក់ព័ន្ធនឹងស្ថានភាពសេរីភាព បញ្ចេញមតិនិងលទ្ធិប្រជាធិបតេយ្យនៅកម្ពុជា</h5>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @for ($i=1;$i<=10;$i++)
                            <div class="col-md-6 mb-3">
                                <div class="card card-horizontal-xs">
                                    <a href="#">
                                        <img class="card-img-top" src="{{ asset('img/banner_01.jpg') }}" alt="{{ $host_name }}">
                                    </a>
                                    <div class="card-body p-md-3 p-2">
                                        <a href="#">
                                            <h5 class="card-title mb-0">គណៈប្រតិភូកម្ពុជាសូមប្រើប្រាស់សិទ្ធិឆ្លើយតបទៅនិងគណៈប្រតិភូនៃចក្រភពអង់គ្លេសដែលបានថ្លែងពាក់ព័ន្ធនឹងស្ថានភាពសេរីភាព បញ្ចេញមតិនិងលទ្ធិប្រជាធិបតេយ្យនៅកម្ពុជា</h5>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endfor
                        </div>
                    </div>
                </div>
                <!-- sidebar -->
                <div class="col-md-4">
                    <!-- include sidebar -->
                    @include('templates.sidebar')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <!-- page js -->
    <script type="text/javascript" src="{{ asset('js/'.strtolower(Route::currentRouteName()).'.js?v='.$version) }}"></script>
@endsection
